require 'spec_helper'

describe 'pricing page', :js do
  before do
    visit '/pricing'
  end

  context 'when looking at the offerings' do
    context 'the page starts with gitlab.com plans' do
      it 'shows the pane with the gitlab-com id as active' do
        expect(find('.tab-pane.active')[:id]).to eq('gitlab-com')
      end

      it 'sends you to the gitlab.com signup form when clicking at the free plan' do
        expect(find('.plan--free .plan-actions a')[:href]).to eq('https://gitlab.com/users/sign_in')
      end

      it 'contains a total of 4 plans with their characteristics' do
        expect(page.assert_selector('.tab-pane.active .plan', count: 4)).to be_truthy
      end
    end

    context 'when looking at the self-hosted offerings' do
      before do
        click_link('Self-Managed', { href: '#self-managed' })
      end

      it 'shows the pane with the self-hosted id as active' do
        expect(find('.tab-pane.active')[:id]).to eq('self-managed')
      end

      it 'sends you to the install page for the core plan' do
        expect(find('.plan--core .plan-actions a')[:href]).to include('/install')
      end

      it 'contains a total of 4 plans with their characteristics' do
        expect(page.assert_selector('.tab-pane.active .plan', count: 4)).to be_truthy
      end
    end
  end

  context 'when looking at the CTA' do
    it 'is able to contact sales' do
      expect(page).to have_css('.contact-block')
      expect(page).to have_selector(:link_or_button, 'Contact Sales')
      expect(find('.contact-block a')[:href]).to include('/sales')
    end
  end

  context 'when looking at the FAQ' do
    it 'contains a list of questions' do
      expect(page.assert_selector('.faq-item', minimum: 1)).to be_truthy
    end

    it 'shows the contents of a question when it is clicked' do
      first('.faq-item .js-faq-question').click

      expect(page.assert_selector('.faq-item.is-open', count: 1)).to be_truthy
    end
  end
end
