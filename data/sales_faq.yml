questions:
  - question: How do I purchase additional users for a license I already bought?
    answer: |
      GitLab follows a true-up model that allows you to have more users than you paid for, and
      then pay for them when you renew your license. You can read more about it on our
      <a href="/pricing/licensing-faq/#can-i-add-more-users-to-my-subscription">license FAQ</a>.
  - question: How much does a license for GitLab cost?
    answer: |
      Pricing information is listed on our <a href="/pricing/">pricing page</a>.
  - question: What is the difference between Core, Starter, Premium, Ultimate, and GitLab.com?
    answer: |
      All of the features and benefits of the different GitLab offerings can be found on the
      <a href="/pricing/">pricing page</a>.
  - question: What are the advantages of each GitLab offering?
    answer: |
      The main benefits of each offering can be found on our
      <a href="/pricing/">pricing page</a>.
  - question: What tools does GitLab integrate with?
    answer: |
      GitLab offers a number of third-party integrations. Documentation on which ones and how to
      integrate them can be found in
      <a href="https://docs.gitlab.com/ee/integration/README.html">our documentation on integrations</a>.
  - question: How do I install GitLab using a container?
    answer: |
      You can find out about installing GitLab using Docker in
      <a href="https://docs.gitlab.com/omnibus/docker/README.html">our documentation</a>.
  - question: What is the difference between GitLab and (other Git solution)?
    answer: |
      You can see all the differences between GitLab and other popular Git solutions on
      <a href="/devops-tools/">our comparison page</a>.
  - question: Do you offer any discounts for GitLab?
    answer: |
      GitLab Core is open source and completely free for anyone to use. We don’t
      currently offer any discounts for the Enterprise subscriptions.
  - question: How do I migrate to GitLab from (other Git tool)?
    answer: |
      You can see all the migration instructions to move to GitLab from other
      popular version control systems in
      <a href="https://docs.gitlab.com/ee/workflow/importing/README.html">our documentation</a>.
  - question: |
      Is GitHost still available?
    answer: |
      No, we are no longer accepting new customers for GitHost. More information is available in the <a href="/gitlab-hosted/">GitHost FAQ</a>
  - question: How do I upgrade from GitLab Core to one of the Enteprise subscriptions?
    answer: |
      If you want to upgrade from GitLab Core to one of the Enterprise subscriptions, you can follow the
      <a href="https://docs.gitlab.com/ee/update/README.html#community-to-enterprise-edition">guides in our documentation</a>.
  - question: As an unofficial reseller, what happens after I place an order on behalf of my customers?
    answer: |
      GitLab will process the order and send the customer a EULA request directly to the email address provided.
      Once they have accepted the terms of the EULA, the license key will be sent directly to them.
