---
layout: markdown_page
title: "GitLab Unleashes the Power of Kubernetes in the Developer Workflow, Helping Enterprises Scale Quickly"
---

### GitLab Unleashes the Power of Kubernetes in the Developer Workflow, Helping Enterprises Scale Quickly

_Building on GitLab’s GKE Integration, GitLab’s Single Application Extends Kubernetes Through the Entire DevOps Lifecycle_

SAN FRANCISCO, CA — April 16, 2019 — Today, GitLab, the single application for the DevOps lifecycle, is making it easy for businesses to leverage the power of Kubernetes. With a built-in container registry and Kubernetes integration, GitLab makes it simple to get started with containers and cloud-native development to optimize application development processes. With GitLab, Kubernetes is accessible to each application developer in the organization, accelerating the software development process. By integrating Kubernetes into the developer workflow, GitLab is helping invigorate developer teams to drive innovative software to production.

“By allowing people to quickly connect Kubernetes clusters to their projects we are helping many enterprises embrace the cloud-native way of building applications,” said Sid Sijbrandij, CEO at GitLab. “By providing a single application we allow enterprise developer and operations teams to embrace Kubernetes every step of the way in their software development process. We’ve seen a large financial institution go from a single build every two weeks to over 1,000 self-served builds a day using GitLab. It is wonderful to see the scale we can unlock for organizations by providing access to Kubernetes in the developer workflow.”

Enterprise teams can easily connect any existing Kubernetes cluster on any platform to GitLab by using GitLab’s native Kubernetes integration. GitLab even makes it easy to set up and configure new clusters with just a few clicks using Google Kubernetes Engine (GKE) integration. Once connected, teams can install managed applications like Helm Tiller, Ingress, and Prometheus to their cluster with a single click in the GitLab interface. Connected clusters are available as a deploy target from GitLab CI/CD and are monitored using GitLab’s bundled Prometheus capabilities. The Kubernetes integration gives you access to advanced features like:

* [Deploy Boards](https://docs.gitlab.com/ee/user/project/deploy_boards.html) - A consolidated view of the current health and status of each CI/CD environment running on Kubernetes, displaying the status of the pods in the deployment.
* Advanced Deployments - Popular Continuous Delivery strategies like [Canary Deployments](https://docs.gitlab.com/ee/user/project/canary_deployments.html) and [Incremental Rollout](https://docs.gitlab.com/ee/topics/autodevops/#incremental-rollout-to-production-premium) test traffic with a small portion of the fleet before deploying the new version of your application more broadly.
* [Kubernetes monitoring](https://docs.gitlab.com/ee/user/project/clusters/#monitoring-your-kubernetes-cluster-ultimate) - Via Prometheus, the CNCF monitoring project, GitLab provides support for automatically detecting and monitoring Kubernetes clusters as well as the ability to configure custom metrics.
* [Auto DevOps](https://about.gitlab.com/product/auto-devops/) - A default CI/CD setting automatically generates pipelines with no configuration needed. Based on best practices Auto DevOps pipelines automatically detect, build, test, secure, deploy, and monitor applications and are fully customizable to tailor CI/CD for any needs.
* [Interactive Web Terminals](https://docs.gitlab.com/ee/ci/interactive_web_terminal/#interactive-web-terminals) - instant web access to a terminal for remote environments makes troubleshooting easy.

“As enterprises adopt cloud-native software development, we’re seeing growing momentum for Kubernetes-native continuous delivery and deployment tools,” said James Governor, analyst and co-founder of RedMonk. “GitLab is well positioned as an end to end stack for modern software delivery in this context.”

"GitLab has provided a great foundation as we have built out cncf.ci to provide cross-project continuous integration and interoperability testing," said Dan Kohn, executive director of the CNCF. "We are excited to see them integrate many CNCF-hosted projects in their platform and look forward to strengthening our collaboration going forward."

#### About GitLab
GitLab is a single application built from the ground up for all stages of the DevOps lifecycle for Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle, allowing teams to collaborate and work on a project from a single conversation, significantly reducing cycle time and focus exclusively on building great software quickly. Built on open source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network, and Comcast trust GitLab to deliver great software at new speeds.

#### Media Contact
Nicole Plati
<br>
gitlab@highwirepr.com
<br>
415-963-4174 ext. 39
