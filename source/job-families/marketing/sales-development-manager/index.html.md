---
layout: job_family_page
title: "Sales Development Manager"
---

As a Sales Development Manager, you are a player-coach. As a Manager your job is threefold: (1) Lead from the front and  generate qualified opportunities for the sales team, (2) Train other members of the sales development team, and (3) take on operational and administrative tasks to help the sales development team perform and exceed expectations. You will be a source of knowledge and best practices amongst the outbound SDRs, and will help to train, onboard, and mentor new SDRs.

## Responsibilities

* Train other members of the Sales Development Team to identify, contact, and create qualified opportunities.
* Ensure Sales Development Team members improve performance and abilities over time by providing coaching and feedback in recurring 1:1s
* Assist with recruiting, hiring, and onboarding new Sales Development Representatives.
* Work closely with the Online Marketing Manager on targeted ad campaigns for the team’s account list
* Work closely with the Sales and Business Development Manager to improve opportunity management and qualification processes
* Work closely with the Sales and Business Development Manager as well as the Regional Sales Directors (RDs) to identify key company accounts to develop.
* Work in collaboration with Content and Product Marketing to develop effective messaging for outbound communications to your assigned accounts.

## Requirements

* Excellent spoken and written English
* Experience in sales, marketing, or customer service for a technical product - leadership experience is highly preferred.
* Experience with CRM software (Salesforce preferred)
* Experience in sales operations and/or marketing automation software preferred
* Understanding of B2B software, Open Source software, and the developer product space is preferred
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Passionate about technology and learning more about GitLab
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

### Levels

## Senior Manager Acceleration

## Description
As a SR. Manager of the Sales Development Acceleration team, you are a player-coach. As a Manager your job is threefold: (1) Lead from the front (as the “tip of the spear”) and generate qualified opportunities for the sales team, (2) train other members of the sales development team, and (3) take on operational and  administrative tasks to help the sales development team perform and exceed expectations. You will be a source of knowledge and best practices amongst the outbound SDRs and will help to train, onboard, and mentor new SDRs.
This team will be focused on the Enterprise Acceleration space.  The team will be responsible for developing plans in conjunction with field and digital marketing to light up underserved territories and execute to pipeline deficiencies as needed.  The successful candidate will have the ability to determine the go to market strategy as well as hire the initial global team.  This is an opportunity to build a new functionality within the GitLab Revenue Marketing team.

## Responsibilities
- Build and train other members of the Sales Development Team to identify, contact, and create qualified opportunities
- Determine a strategy for acquisition and create a process for execution
- Help to define performance metrics by which SDR’s will be measured
- Establish a team of experienced hunters
- Create a process for continuous development and onboarding new hires 
- Coach SDR’s through shadowing, role plays, process improvement, and performance reporting in recurring 1:1’s
- Work closely with the Online Marketing Manager on targeted ad campaigns for the team’s account list
- Work closely with the Sales and Business Development Managers to improve opportunity management and qualification processes
- Work closely with the Sales and Business Development Managers as well as the Regional Sales Directors (RDs) to identify key company accounts to develop
- Work in collaboration with Content and Product Marketing to develop effective messaging for outbound communications to your assigned accounts
- Collaborate with Marketing Operations to identify new tools and programs that could help improve output and revenue generation
- Assist with recruiting, hiring, and onboarding new Sales Development Representatives

## Requirements
- A prospecting mindset
- Proven success in acquisition and closing roles 
- Basic understanding of marketing programs and field marketing concepts
- Coaching or management experience
- Experience with CRM software and prospecting tools like Outreach, Drift, Discover.org etc.
- Experience in sales operations and/or marketing automation software preferred
- Understanding of B2B software, Open Source, and the developer product space is preferred
- Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
- You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
- Passionate about technology and learning more about GitLab
- Be ready to learn how to use GitLab and Git
- You share our values, and work in accordance with those values.
- Leadership at GitLab





## Specialties

### Tanuki Tribe

As a Sales Development Manager, Tanuki Tribe, you are a trainer and coach. As a Manager your job is threefold: (1) Onboard, mentor and train SDR and BDR new hires, (2) Train other members of the sales development team, and (3) take on operational and administrative tasks to help the sales development team perform and exceed expectations. You will be a source of knowledge and best practices amongst the BDRs and SDRs.

#### Responsibilities

* Train other members of the Sales Development Team to identify, contact, and create qualified opportunities.
* Ensure Sales Development Team members improve performance and abilities over time by providing coaching and feedback in recurring 1:1s
* Assist with recruiting, hiring, and onboarding new Sales Development Representatives.
* Work closely with the Product Marketing on our weekly Enablement series
* Work closely with the Sales and Business Development Managers to improve opportunity management and qualification processes
* Work closely with the Director of Sales Development as well as the Regional Sales Directors (RDs) to identify key trends to keep the team current on market positioning.
* Work in collaboration with Content and Product Marketing to develop effective messaging for outbound communications to the teams assigned accounts.


**Note on Compensation**

See the [Sales Compensation Plan](/handbook/finance/sales-comp-plan/) to understand OTE salary.
