---
layout: markdown_page
title: "SLC.1.01 - Service Lifecycle Workflow Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SLC.1.01 - Service Lifecycle Workflow

## Control Statement

Major software releases are subject to the Service Life Cycle, which requires acceptance via Concept Accept and Project Plan Commit phases prior to implementation.

## Context

The purpose of this control is to formalize the documentation and approval of software changes before those changes are implemented. This rigid process helps protect GitLab from insecure code being quickly pushed out into production without proper vetting.

## Scope

This control applies to all major software releases to GitLab's SaaS offering.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SLC.1.01_service_lifecycle_workflow.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SLC.1.01_service_lifecycle_workflow.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SLC.1.01_service_lifecycle_workflow.md).

## Framework Mapping

* ISO
  * A.14.1.1
  * A.14.2.5
* SOC2 CC
  * CC8.1
* PCI
  * 6.3
