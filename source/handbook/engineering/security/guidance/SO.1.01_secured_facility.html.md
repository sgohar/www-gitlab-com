---
layout: markdown_page
title: "SO.1.01 - Secured Facility Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SO.1.01 - Secured Facility

## Control Statement

Physical access to restricted areas of the facility is protected by walls with non-partitioned ceilings, secured entry points, and/or manned reception desks.

## Context

This control applies to all GitLab facilities and anywhere that has infrastructure relating to the GitLab corporate network.

## Scope

This control is not applicable to GitLab since there are no facilities and no corporate network.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.1.01_secured_facility.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.1.01_secured_facility.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/SO.1.01_secured_facility.md).

## Framework Mapping

* ISO
  * A.11.1.1
  * A.11.1.2
  * A.11.1.3
  * A.11.1.4
  * A.11.1.5
  * A.11.1.6
  * A.11.2.1
* SOC2 CC
  * CC6.4
* PCI
  * 9.1
  * 9.1.3
  * 9.5
