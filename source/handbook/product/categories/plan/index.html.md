---
layout: markdown_page
title: "Plan Stage"
---

## On this page

{:.no_toc}

- TOC
{:toc}

### Plan

{: #welcome}

Plan teams:

* [Plan:Project Management Backend Team](/handbook/engineering/development/dev/plan-project-management-be/)
* [Plan:Portfolio Management Backend Team](/handbook/engineering/development/dev/plan-portfolio-management-be/)
* [Plan:Certify Backend Team](/handbook/engineering/development/dev/plan-certify-be/)
* [Plan Frontend Team](/handbook/engineering/development/dev/fe-plan/)

The responsibilities of this collective team are described by the [Plan stage](/handbook/product/categories/#plan-stage). Among other things, this means
working on GitLab's functionality around issues, issue boards, milestones, todos, issue lists and filtering, roadmaps, time tracking, requirements management, and notifications.

- I have a question. Who do I ask?

In GitLab issues, questions should start by @ mentioning the Product Manager for the [corresponding Plan stage group](/handbook/product/categories/#plan-stage). GitLab team-members can also use [#g_plan](https://gitlab.slack.com/messages/C72HPNV97).

### How we work

- In accordance with our [GitLab values](/handbook/values/).
- Transparently: nearly everything is public, we record/livestream meetings whenever possible.
- We get a chance to work on the things we want to work on.
- Everyone can contribute; no silos.
- We do an optional, asynchronous daily stand-up in [#g_plan_standup](https://gitlab.slack.com/messages/CF6QWHRUJ).

### Workflow

We work in a continuous Kanban manner while still aligning with Milestones and [GitLab's Product Development Flow](/handbook/product-development-flow/).

#### Issues

Issues have the following lifecycle. The colored circles above each workflow stage represents the emphasis we place on collaborating across the entire lifecycle of an issue; and that disciplines will naturally have differing levels of effort required dependent upon where the issue is in the process. If you have suggestions for improving this illustration, you can leave comments directly on the [whimsical diagram](https://whimsical.com/2KEwLADzCJdDfPAb2CULk4).

![plan-workflow-example.png](plan-workflow-example.png)

#### Epics

If an issue is `> 3 weight`, consider promoting it to an epic (quick action) and split it up into multiple issues. It's helpful to add a task list with each task representing a vertical feature slice (MVC) on the newly promoted Epic. This enables us to practice "Just In Time Planning" by creating new issues from the task list as there is space downstream for implementation. When creating new vertical feature slices from an epic, please remember to add the appropriate labels - `devops::plan`, `group::*`, `Category:*` or `feature label`, and the appropriate `workflow stage label` - and attach all of the stories that represent the larger epic. This will help capture the larger effort on the roadmap and make it easier to schedule.

#### Roadmap Organization

```mermaid
graph TD;
  A["devops::plan"] --> B["group::*"];
  B --> C["Category:*"];
  B --> D["non-category feature"];
  C --> E["maturity::minimal"];
  C --> F["maturity::viable"];
  C --> G["maturity::complete"];
  C --> H["maturity::lovable"];
  E--> I["Iterative Epic(s)"];
  F--> I;
  G --> I;
  H --> I;
  D --> I;
  I--> J["Issues"];
```

### Retrospectives

The Plan stage conducts [monthly retrospectives in GitLab
issues][retros]. These are confidential during the initial discussion,
then made public in time for each month's [GitLab retrospective]. For
more information, see [team retrospectives].

The retrospective issue is created by a scheduled pipeline in the
[async-retrospectives] project. For more information on how it works, see that
project's README.

[GitLab retrospective]: /handbook/engineering/workflow/#retrospective
[team retrospectives]: /handbook/engineering/management/team-retrospectives/
[async-retrospectives]: https://gitlab.com/gitlab-org/async-retrospectives
[retros]: https://gitlab.com/gl-retrospectives/plan/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective

### Meetings

Most of our group meetings are recorded and publicly available on
YouTube in the [Plan group playlist][youtube].

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL05JrBw4t0KoceqcTneOVmAzhEp6NinY0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

[youtube]: https://www.youtube.com/playlist?list=PL05JrBw4t0KoceqcTneOVmAzhEp6NinY0

#### Links / References

`~devops::plan` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1226305) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan)

`~group::project management` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1235826) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Aproject%20management)

`~group::portfolio management` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1224651) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Aportfolio%20management)

`~group::certify` - [Kanban Board](https://gitlab.com/groups/gitlab-org/-/boards/1235846) • [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Aplan&label_name[]=group%3A%3Acertify)

#### Process Improvements We're Working Towards

- **Smaller Iterations:** A 4-6 weeks release cycle is helpful from a marketing perspective, but it is too long of a time horizon for continuously delivering working software incrementally and iteratively. We can accomplish this by breaking bigger features intto smaller vertical feature slices. If an Issue has a `weight` of `> 3`, it is a good signal it should be [broken down into smaller pieces](https://www.youtube.com/watch?v=EDT0HMtDwYI). The larger the weight, the more risk and likelihood the Issue will take longer than the estimate.
- **Limit Work In Progress:** [Context switching is expensive](https://blog.codinghorror.com/the-multi-tasking-myth/). By limiting the amount of Issues in progress at any given time, the less frequently you will have to incur this cost. You may think multitasking is inevitable, but by adhering to a work in progress limit, you will more quickly surface the parts of our process that are inefficient; enabling us to collectively fix them as a team.
