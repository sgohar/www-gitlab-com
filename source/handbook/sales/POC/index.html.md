---
layout: markdown_page
title: "GitLab Proof of Concept"
---

# Proof of Concept (POC) / Proof of Value (POV) Guidelines
{:.no_toc}

GitLab wants prospects to enjoy a successful Proof of Concept (also known as Proof of Value) with GitLab Enterprise Edition. A POC is a collaboration between GitLab and the prospective customer for evaluating GitLab Enterprise Edition. As a best practice, GitLab product evaluations should remain separate from GitLab high availability architecture and implementation evaluations. Due to cost and time intensity, a POC should not be the default course of action for most GitLab buyers. POC's should focus on specific customer business outcomes that cannot be achieved through other technical and/or business interactions.  

POC's are commonly entered into once an NDA is signed, budget for the project is in place, any competition has been identified and success criteria have been documented.

The target duration for a POC is between 1 and 8 weeks depending on complexity and style of engagement. A GitLab Customer Success [collaborative project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) is the default method for POC management. The project template is only accessible by GitLab team members, but once a collaborative project is created, the customer will be granted access to that project. When utilizing a collaborative project, follow the instructions in README.md closely in order to properly copy and configure the entire project. If a prospect has an internal process to follow or cannot use GitLab.com to manage the POC, a [Guided POC document](#POCdoc) or a simplified [Lite POC document](#LITEdoc) may be used.

GitLab Solutions Architects should consider a limit of 3 concurrent Guided or Paid POC's as a best practice in order to provide excellent customer service and to enable a target of achieving 24 hour or less response times to inquiries during any POC. If more than 3 concurrent Guided or Paid POC's are required, the SA should assess their ability to support the additional requirements and/or discuss viability with their manager as well as other SA's in their region.

## On this page
{:.no_toc}

- TOC
{:toc}

## Tracking a POC in Salesforce

### Salesforce Object

In order to track a POC correctly in Salesforce, the Strategic Account Leader should position the opportunity as Stage 3. The Solutions Architect will create the POC object within SFDC when the prospect or customer has indicated interest in moving forward with a guided or lite POC. 

To track a POC, click the *Proof of Concepts* tab from the top menu bar in Salesforce. Create a new POC using the *New* button. Alternately, the Solutions Architect may select the relevant Opportunity, scroll down to the related list labeled *Proof of Concepts* and click on the "New Proof of Concept" button. This will automatically associate the POC with that Opportunity while all other fields need to be manually completed. 

Complete the following fields at minimum:  
  
  * **POC Owner** - this is the Strategic Account Leader
  * **Technical Account Manager** - this is the TAM who will be part of the POC
  * **Solutions Architect** - this is the SA who owns execution of the POC
  * **Proof of Concept Name** - this commonly is named identical to the opportunity name
  * **Account** - this is the company name
  * **Opportunity** - this is the associated opportunity name (this opportunity should be in stage 3)
  * **POC Start Date** - the date the POC is expected to begin
  * **POC Close Date** - the date the POC is expected to End
  * **POC Type** - this is style POC being executed

Once the POC begins, the Solutions Architect should change the **Status** field from *New* to *In Progress*. When the POC is complete, the Solutions Architect should change the **Status** to *Closed* and the **Result** should be identified as *Successful* or *Unsuccessful*. Freeform notes should be added to support the reason for the successful or unsuccessful result.

## POC Best Practices

Solutions Architects are the owners of the POC, guiding prospects through a successful experience with GitLab. As such, Solutions Architects should be the primary contacts for questions and issues experienced by the prospect during the POC. If technical problems arise which are unexpected, after evaluation by the SA and verification with the prospect, the SA may engage support via the trials@gitlab.zendesk.com email address. This should only be used for technical abnormalities, not for integration or implementation assistance.  

Many prospects are tempted to include implementation of GitLab high availability as part of a POC. In this case, the implementation components should comprise a separate POC entirely, separating GitLab functionality evaluations from implementation, load and performance components. 

Below is best practice guidance for conducting each type of POC. These processes share a similar goal of meeting identified business outcomes for the customer, but they vary based on engagement style, POC duration, location and intensity. The following applies to all POC's:

### POC Key Players

#### Customer Roles 

*  Executive contact - Someone with business level, budgetary buy in
*  At least one pilot team to execute the POC
*  Technical POC lead

#### GitLab Roles

*  Strategic Account Leader (SAL) or Account Executive (AE) - Relationship manager, owns temporary license, POC kickoff, SFDC admin, scheduling
*  Solutions Architect (SA) - Primary technical contact, POC lead and project manager
*  Technical Account Manager (TAM) - *Only for qualified accounts.* Included in communication and select calls for customer visibility, assists technically as needed
*  Professional Services - rarely as needed in a pre-sales capacity
*  Support Team - only if needed for technical errors, engaged via trials@gitlab.zendesk.com by the customer per the [Internal Support page](/handbook/support/internal-support/#note-on-zendesk-and-supportgitlabcom)

### POC Kickoff Checklist

* SA: Ensure the customer architecture is prepared to support the POC (if on-premises)
* SA: Ensure customer network has access to GitLab.com (if SaaS evaluation)
* SA: Customer Success project is created in GitLab as per the [TAM Handbook page](/handbook/customer-success/tam/#to-start-a-new-customer-engagement)
* SA: POC document is created if this is required by the customer, otherwise default to the Customer Success project
* SA: Ensure POC goals and business outcomes are clearly identified prior to kickoff
* SA: For the largest strategic opportunities, notify GitLab Support of POC dates, customer, and other relevant information using the applicable Slack channel related to Self-Managed or GitLab.com support
* SAL/AE: Opportunity updated in Salesforce, set to Stage 3-Technical Evaluation, with POC Information entered per the [handbook](/handbook/business-ops/#opportunity-stages)
* SAL/AE: Signed NDA by the legal team if required
* SAL/AE: Schedule Internal kick off meeting (detailed below)
* SAL/AE: Schedule kickoff meeting with customer
* TAM: Review collaborative project content prior to internal kickoff meeting

### POC Meeting Recordings

POC-related calls may be recorded with customer consent. Recordings may be stored in Chrous, in a folder on Google Drive (if recorded locally), or within the project repository (if small). Any recording links should be identified in the notes stored within the Documents directory of the project repository. 

## POC Types

There are multiple approaches to a POC. The POC type chosen should reflect the wishes and best fit for the client. POC types include:

  * **On-site** - This is an intense, condensed POC typically executed on the client premises within a tight evaluation window of 1 to 2 weeks
  * **Guided** - This is the most common POC for enterprise customers, typically lasting 14 to 60 days
  * **Paid** - In this case, a prospect pays GitLab for a long duration (>60 days) or high attention (formally dedicated percentage of an SA's/TAM's time) or extended time evaluation of the software (more than 60 days)
  * **Lite** - This type of POC is commonly used for smaller opportunities requiring minimal touch points throughout the POC or in situations where a client has their own POC process which must be adhered to. A variant of this process is commonly executed within Mid-Market environments. 

Best practices specific to each type of POC follows.

### On-site POC

The On-site POC is typically the shortest and most intense POC. It is critical that before this type of POC begins:

* All outcomes and objectives are agreed to and documented 
* GitLab and customer team members are identified
* Calendar schedules are arranged
* System installations and/or user administration are complete

The SA will typically join the client at their chosen location and work directly with the team there to quickly identify the value proposition of GitLab within their environment. The SA will dedicate themselves to the client for 3 to 4 days a week for this POC, collaborating on value drivers, assisting in solving problems, and enabling customer POC owners on required knowledge to obtain identified POC outcomes. 

### Guided POC 

The Guided POC is the most commonly utilized type of POC for Enterprise accounts. These will commonly have a 30 to 60 day duration. These POC's are marked by regular touch points and consistent interaction over time without requiring full time dedication to the GitLab evaluation on behalf of of the customer. It is common to have kickoff meetings, technical support calls, weekly retrospective calls and POC conclusion calls when running a Guided POC. These meetings may be represented by the following suggestions:

#### Internal Kickoff Meeting, led by the Solutions Architect

GitLab Attendees: Strategic Account Leader, Solutions Architect, Technical Account Manager

Agenda:
*  Collaboratively review customer success project README to ensure everything is correct
*  POC document review (only if a document is required for the POC)
*  Discussion of strategy, whether GitLab Support or Professional Services need to be notified
*  Strategic Account Leader to schedule external kickoff with customer

#### External Kickoff Meeting (Remote), led by the Solutions Architect

Attendees:
*  GitLab: Strategic Account Leader, Solutions Architect, Technical Account Manager
*  Customer: Executive contact, Technical POC lead

Agenda:
*  Screen share with the customer to discuss the below agenda items
*  Collaboratively review customer success project README to ensure everything is correct
*  Agree on due dates for POC completion, add to project POC milestone
*  Review project POC issues - add new issues if necessary with agreed due dates
*  Review POC document with customer (only when required)
*  Establish cadence with customer, Strategic Account Leader to schedule during this call
*  Create issues for each cadence call with customer under the POC milestone for call notes
*  Provision licenses and establish if customer needs help getting GitLab set up and configured

#### Weekly Retrospective call, led by the Solutions Architect

Attendees:
*  GitLab: Solutions Architect, Strategic Account Leader, Technical Account Manager (optional)
*  Customer: Technical POC Lead

Agenda:
*  What went well?
*  What problems do we need to overcome?
*  What do we need to change?
*  Review success criteria - are we on track?

#### POC Conclusion Meeting - Led by the Strategic Account Leader

Attendees:
*  GitLab: Strategic Account Leader, Solutions Architect, Technical Account Manager
*  Customer: Executive contact, Technical POC Lead

Agenda:
* Did we meet the success criteria?
* Did we meet the goals for the customer? Is this a technical win?
* Next steps
* Send out POC survey

### Paid POC 

The Paid POC is less common than other types of POC's. These will commonly have duration greater than 60 days, and the customer will pay for usage of GitLab for the duration of the POC. Before this type of POC can begin, it requires a GitLab prorated licensing purchase to be completed. These POC's are marked by regular touch points and consistent interaction over time without requiring full time dedication to the GitLab evaluation on behalf of the customer. This type of POC will commonly have a larger ecosystem focus, where the value of GitLab is dependent on interactions with other tools and environments within the client's ecosystem. It is common to have kickoff meetings, technical support calls, weekly retrospective calls and POC conclusion calls similar to those identified for a Guided POC. 

### Lite POC

When a prospect has an internal POC process to follow, or when time is of the essence, a Lite POC is chosen.

#### <a name="LITEdoc"></a>Lite POC Template Document

For a Lite POC, the SA may utilize the [Lite POC document](https://docs.google.com/document/d/1PO3jXG3wiKsCbx5vb8dm4SmOe_PiTB47SadROIO8nCc/edit#) (only accessible to GitLab team-members), which validates POC dates, success criteria and assumptions of both GitLab and the prospect.

In the case of a "Lite" POC, the Solutions Architect is expected to be the sole GitLab contact. "Lite" is determined case-by-case by the size of the prospect as well as their ability to engage with GitLab.

#### Using the LITE POC Template

The template provides a simplified approach to the POC process, but the document requires customization for each POC to be executed.  

To use the template, begin by making a copy of the master document for each POC.  

Edit each area highlighted in yellow within the document to include pertinent information for any particular prospect. This information includes basic data like the prospect name and GitLab team-member details, as well as data to be collaboratively identified with the prospect, such as success criteria, trial location and the client pilot team(s). Delete any red-colored instructional text.  

Finally, ensure both GitLab and the prospect have a copy of the document. Schedule weekly meetings for the duration of the POC via calendar invites prior to distributing the GitLab Enterprise Edition license for the POC.

## Commercial Sales POC Guide

Commercial Sales POC's are commonly executed as a variety of the Lite POC, though they may not utilize the [Lite POC document](https://docs.google.com/document/d/1PO3jXG3wiKsCbx5vb8dm4SmOe_PiTB47SadROIO8nCc/edit#). Common customer interactions for Commercial POC's are identified below.

### Kick Off Meeting
* Duration: 30 Minutes
* Attendees: GitLab Account Executive, GitLab Solutions Architect, Prospective Buyer(s)
* Agenda:
  * Define success criteria (as a best practice, have no more than 5 unique success criteria deliverables)
  * Confirm start and end date
  * Determine communication method
    * Customer to decide if they are interested in communicating on a collaborative GitLab project or email only
    * Frequency options: 30 minute weekly call or email touchbase weekly with calls scheduled as needed
  * Outline the SA role
    * Acts as the primary point of contact throughout POC process
    * Tracks status updates regarding success criteria
    * Records any existing or new feature requests of interest to customer
    * Leads troubleshooting and escalation of blocks

### Commercial Sales - Customer Success Plan Creation
* [Track POC in Salesforce according to this process](/handbook/sales/POC/#tracking-a-proof-of-concept-in-salesforce)
* Create [POC project](https://gitlab.com/gitlab-com/account-management/commercial/pre-sales) in Commercial/Pre-sales group by selecting Create from Template > Group > Project-Template-Commercial-Sales
* Edit README.md with information specific to POC under Proof of Concept section
* Upon completion of POC, update Salesforce record with POC result as successful or unsuccessful and provide supportive reasons in the associated freeform fields
  * For a successful POC:
    * SA to move project from Pre-sales folder to [Commercial](https://gitlab.com/gitlab-com/account-management/commercial)
    *  SA and AE to schedule meeting with new customer to walk through README.md. The intended outcome is to confirm the account information specific to their GitLab instance is accurate in the Customer Success Plan.
    * TAM to determine TAM eligibility and engagement based on Account Tier and IACV.

## <a name="POCdoc"></a>POC Template Document

As an alternative (or in addition) to using a collaborative GitLab project, a document is available which helps outline the details of a POC. In the [document](https://docs.google.com/document/d/1anbNik_H_XDHJYyZPkfr4_6wvIXgqbwvWynN9v9tj2E/edit#) (only accessible to GitLab team members), provides the framework for a successful POC by addressing current state issues, persistent challenges, business problems, desired state and outcomes.  

This document suggests and verifies specific success criteria for any POC, as well as outlining a mutual commitment between GitLab and the identified prospect parties. It also specifies the limited timeframe in which the POC will occur.  

### Using the POC Template

The template provides a standardized approach to the POC process, but the document requires customization for each and every POC to be executed.  

To use the template, begin by making a copy of the master document for each POC.  

Edit each area highlighted in yellow within the document to include pertinent information for any particular prospect. This information includes basic data like the prospect name and GitLab team-member details, as well as data to be collaboratively identified with the prospect, such as success criteria, trial location, and the client pilot team(s).  

After all the highlighted sections have been completely filled in, collaboratively complete the **Date** and **Owner** column fields within the **Project Plan** and **Roles and Responsibilities** sections.  

Finally, ensure both GitLab and the prospect have a copy of the document. Schedule all meetings associated to the POC via calendar invites prior to distributing the GitLab Enterprise Edition license for the POC.


