---
layout: markdown_page
title: "Spending Company Money"
---

## On this page
{:.no_toc}

- TOC
{:toc}

In keeping with our values of results, freedom, efficiency, frugality, and boring solutions, we expect GitLab team-members to take responsibility to determine what they need to purchase or expense in order to do their jobs effectively. We don't want you to have to wait with getting the items that you need to get your job done. You most likely know better than anyone else what the items are that you need to be successful in your job. The guidelines below describe what people in our team commonly expense. Some of the items below have the average amount that folks typically spend on them as a helpful guide. If you need to spend more to get the right tool for you to work productively, please do so. If you are uncertain about how much reasonable to spend on a more expensive item, please don't hesitate to reach out and ask for help.

1. Spend company money like it is your **own** money. _No, really_.
1. You don't have to [ask permission](https://m.signalvnoise.com/if-you-ask-for-my-permission-you-wont-have-my-permission-9d8bb4f9c940) before making purchases **in the interest of the company**. When in doubt, do **inform** your manager before the purchase, or as soon as possible after the purchase.
1. It is uncommon for you to need all of the items listed below. Use your best judgement and buy them as you need them. If you wonder if something is common, feel free to ask People Ops (and in turn, People Ops should update the list).
1. It is generally easiest and fastest for you to make any purchases for office supplies yourself and expense them. If you are unable to pay for any supplies yourself, please reach out to People Ops.
1. You may privately use GitLab property, a MacBook for example, to check your private e-mails or watch a movie as long as it does not violate the law, harm GitLab, or interfere with [Intellectual Property](/handbook/general-guidelines/#sts=Intellectual Property). More details can be found in the [internal Acceptable Use Policy](/handbook/people-operations/acceptable-use-policy/).
1. If you make a purchase that will cost GitLab $1000 USD (or over), this is classed as company property, you will be required to return the item(s) if you leave the company.
1. Employees: file your expense report in the same month that you made the purchase in.  Please combine multiple expenses on one report if possible. Contractors: include receipts with your invoices.
1. Any non-company expenses paid with a company credit card will have to be reported to your manager as soon as possible and **refunded** in full within 14 days.
1. **Office Equipment and Supplies.** The company will reimburse for the following items if you **need it for work or use it mainly for business**, and local law allows us to pay for it without incurring payroll taxes. The amounts set forth below are guidelines and not strict limits:
    * Hardware
        * USB-C Adapter for New Macbooks (average price $80 USD)
        * Laptop carrying bag (average price $60 USD)
        * External monitor (average price $380 USD)
        * HDMI cable (make sure you get the proper cable for your laptop) (average price $15 USD)
        * Portable 15" external monitor (average price $200 USD)
        * Monitor Privacy Filter (average price $80 USD)
        * Webcam (average price $50 USD)
        * Ethernet connector (average price $20 USD)
        * [Earpods](https://www.apple.com/shop/product/MNHF2AM/A/earpods-with-35-mm-headphone-plug) are strongly recommended instead of using laptop speakers and microphones since these can cause an echo. (average price $30 USD)
        * Headphones (average price $60 USD)
        * Keyboard (average price $110 USD)
        * Mouse/Trackpad (average price $80/$145 USD)
        * Height-adjustable desk (average price $500 USD)
        * Ergonomic chair (average price $200 USD)
        * Laptop stand (average price $90 USD)
        * [Yubikey](https://www.yubico.com/store/) (average price $50)
    * Software
        * We have central [license management](/handbook/tools-and-tips/#jetbrains) for you to request licenses for JetBrains' products like RubyMine / GoLand
        * We do not issue Microsoft Office 365 licenses, as GitLab uses Google's G Suite (Docs, Slides, Sheets, etc.) instead.
        * For security related software, refer to the security page for [laptop and desktop configuration](/handbook/security/#laptop-or-desktop-system-configuration).
    * Other
        * Business cards ordered from MOO as per the [instructions](/handbook/people-operations/#business-cards) provided by PeopleOps
        * Urgent Business cards needed for day of start can be requested by emailing peopleops@gitlab.com
        * Work-related books
    * **Not sure what to buy?** Look at our [equipment examples page](/handbook/spending-company-money/equipment-examples) to see some of the items that other GitLab team-members have purchased, and please consider adding to the list if there's something you'd like to share.
    * You can also consult the `#questions` channel in slack for recommendations from other GitLab team members.
1. **Expenses.** The company will reimburse for the following expenses if you need it for work or use it mainly for business, and local law allows us to pay for it without incurring taxes:
    * Mileage is reimbursed according to local law: [US rate per mile](http://www.irs.gov/Tax-Professionals/Standard-Mileage-Rates), or [rate per km](http://www.belastingdienst.nl/wps/wcm/connect/bldcontentnl/belastingdienst/zakelijk/auto_en_vervoer/auto_van_de_onderneming/autokosten/u_rijdt_in_uw_eigen_auto) in the Netherlands. Add a screenshot of a map to the expense in Expensify indicating the mileage.
    * Internet connection subscription.
        * For employees in the Netherlands: fill in and sign the [Regeling Internet Thuis](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) form and send it to People Ops. People Ops will then send it to the payroll provider in the Netherlands via email.
    * VPN service subscription. Please read [Why We Don't Have A Corporate VPN](/handbook/security/#why-we-dont-have-a-corporate-vpn) for more information about VPN usage
    at GitLab.
    * Mobile subscription, we commonly pay for that if you call a lot as a salesperson or executive, or if your position requires participation in an oncall rotation. If your device cost is part of your monthly subscription cost, please do not include the device cost from the expense reimbursement.
    * Telephone land line (uncommon, except for positions that require a lot of phone calls)
    * Skype calling credit, we can autofill your account (uncommon, since we mostly use Google Hangouts, Appear.in, Zoom, and WebEx)
    * Google Hangouts calling credit
    * Office space
        * If working from home is not practical for a day, a month, a year, forever, please feel free to rent out a co-working space at the expense of the company. This can include non-traditional spaces that require a membership as long as you average ~4 working days per month at the space. Any agreement must be between the team member and the co-working space (i.e. GitLab will not sign or appear on the agreement). All expenses must be submitted through the normal [travel and expense reimbursement policy](/handbook/finance/accounting/#reimbursable-expenses).  The Company will not be responsible for any expense that relates to office space subsequent to the termination of service between GitLab and the team member.
    * Work-related online courses and professional development certifications. GitLab team members are allotted [$500 USD](/handbook/people-operations/global-compensation/#exchange-rates) per fiscal year to spend on one or multiple training courses. Reimbursement past the [$500 USD](/handbook/people-operations/global-compensation/#exchange-rates) total allotment requires manager approval.
    * The company will pay for all courses related to learning how to code (for example [Learning Rails on Codecademy](https://www.codecademy.com/learn/learn-rails)), and you may also allocate work time to take courses that interest you. If you are new to development, we encourage you to learn Git through GitLab, and feel free to ask any questions in the #git-help Slack channel.
    * Work-related conferences, including travel, lodging, and meals. If total costs exceed [$500 USD](/handbook/people-operations/global-compensation/#exchange-rates), reimbursement requires prior approval from your manager.
        * We encourage people to be speakers in conferences. More information for people interested in speaking can be found on our [Corporate Marketing](/handbook/marketing/corporate-marketing/#speakers) page.
        * We suggest to the attendees bring and share a post or document about the news and interesting items that can bring value to our environment.
    * Year-end Holiday Party Budget: [$50 USD](/handbook/people-operations/global-compensation/#exchange-rates) per GitLab team-member for a holiday in December. We encourage GitLab team-members to self organize holiday parties with those close by.
    * For travel to other team members please see our [visiting grant](/handbook/incentives/#visiting-grant).
    * Business travel upgrades per round-trip (i.e. not per each leg of the flight):
        * Up to the first [$300 USD](/handbook/people-operations/global-compensation/#exchange-rates) for an upgrade to Business Class on flights longer than 8 hours.
        * Upgrade to Economy Plus if you’re taller than 1.95m / 6’5”.
        * Up to the first [$100 USD](/handbook/people-operations/global-compensation/#exchange-rates) for an upgrade to Economy Plus (no height restriction) on flights longer than 8 hours.
    * GitLab does not cover expenses for Significant others or family members for travel or immigration. This includes travel and visas for GitLab events.
    * Something else? No problem, and consider adding it to this list if others can benefit as well.

1. **Expense Reimbursement.**
    * Effective 2019-07-01, all expense reports must be submitted to your manager for approval prior to being sent to Finance for payment.
    * If you are a team member from Nigeria, please submit your expense in your salary invoice (a template can be found [here](/handbook/finance/#invoice-template-and-where-to-send)) with receipts attached to <payroll@gitlab.com>.  Please note, this is a temporary solution while we are transition over to a PEO.
    * If you are a team member and incurred an expense charged in a currency different from the one you use to submit your invoices, use the conversion rates specified in the [global compensation section of the handbook](https://about.gitlab.com/handbook/people-operations/global-compensation/#exchange-rates). If the expense currency doesn’t exist in that list, refer to the conversion rates in [oanda](https://www.oanda.com/currency/converter/). Make sure to set the expense date in the currency converter form.
    * GitLab uses Expensify to facilitate the reimbursement of your expenses. As part of onboarding you will receive an invitation by email to join GitLab's account. Please set up your account by following the instructions in the invitation.
    * If you are a team member in Spain or France, please submit your expenses through Safeguard in-house expense reimbursement management system and also submit them through Expensify.  Payroll will review, approve, and send the approval of your expense reports in Expensify to your gitlab email address.  You will need to forward the approval email to Safeguard enable for them to process your expense reimbursement via payroll.
    * Please make an effort to combine multiple expenses into a single report as it will save the company money by avoiding excessive Expensify fees.
    * If you are new to Expensify and would like a brief review, please see [Getting Started](http://help.expensify.com/getting-started/)
    * For step by step instructions on creating, submitting, and closing a report please see [Create, Submit, Close](http://help.expensify.com/reports-create/)
    * For US team members, the approved expense amount will be deposited into your account a few days after the report has been approved by payroll.
    * For Australia, Belgium, Germany, India, and Netherlands, AP will process the approved report on Friday once payroll approved the report.  The payment will be deposited into your account no later than three business days the following week.
    * For all team members being pay by Safeguard, iiPay, or Vistra, the approved expense amount will be deposited in your account with your monthly salary.
    * If you are a team member with a company credit card, your company credit card charges will automatically be fed to a new Expensify report each month. Please attach receipts for these expenses (per the Expense Policy, see below) within 5 business days after the end of the month. These amounts will not be reimbursed to you but Expensify provides a platform for documenting your charges correctly.
 
  * **Expense Policy**
      * Max Expense Amount - [$5,000 USD](/handbook/people-operations/global-compensation/#exchange-rates)
      * Receipt Required Amount - [$25 USD](/handbook/people-operations/global-compensation/#exchange-rates)

1.  **Approving Expense Reports**
    * Expensify will send a notification email when a team member submitted an expense report
    * Click on the report name in the body of the email
    * Review each expense for the correct amount of the receipt and the report
    * Check for customers or project name if applicable under Tag
    * We required a receipt for any expense greater than $25 (except for Billable policy)
    * Select [Approve and Forward] option and Expensify pre-populated the email address.  Note, Expensify is updating their coding to address a small glitch in this field.  If it is empty, please send it to jnguyen@gitlab.com
    * Important - please do not use [Final Approval] because Expensify will not send the email notification for payment approval and it will delay the reimbursement process
    * Manager can delegate the approval process during PTO:
        *  Settings
        *  Your Account
        *  Vacation Delegate
        *  Enter the email address of the backup approval
    * All expense question(s) can be addressed via payroll@gitlab.com or in the Finance Slack channel 
    
1.  **Expenses Reports approval deadline**
    * Australia, Germany, India, Netherlands, United States - as soon as possible
    * United Kingdom - all expense reports must be approved by manager no later than the 14th of each month.  Team members - please be sure to submit your report(s) couple days before the due date so your manager has enough time for approval.
    * All non-US contractors - all expense reports must be approved by manager no later than the 8th of each month. Team members - please be sure to submit your report(s) couple days before the due date.
        
1.  **Laptops**
    * The [IT Ops](https://about.gitlab.com/handbook/business-ops/it-ops-team/#laptops) page outlines laptop purchasing for new hires and for repairs and EOL for existing employees.
        * Laptops paid for by the company are property of GitLab and need to be reported with serial numbers, make, model, screen size and processor to People Ops by adding it to this form: [GitLab laptop information](https://docs.google.com/forms/d/1XcuK5oZJkdDjt4VE7xYHXPAAb91Dsz8Bsm0ZFJ8mXck/prefill) for proper [asset tracking](/handbook/finance/accounting/#asset-tracking). Since these items are company property, you do not need to buy insurance for them unless it is company policy to do so (for example, at the moment we do not purchase Apple Care), but you do need to report any loss or damage to PeopleOps as soon as it occurs.
1. **Repairs to company issued equipment.**
    * If you need to replace a battery or something small that does not affect the productivity or speed of the device, please go ahead and get that small item replaced and expensed.
    * Please get approval from your Manager if your equipment appears to be damaged, defective, or in need of repair. Business Operations can advise on next steps to ensure you have the proper equipment to work.
    * With GitLab being in [55 countries and growing](/company/team/#countries), please share your experiences in getting your laptops repaired at an authorized dealer in this [issue](https://gitlab.com/gitlab-com/business-ops/it-ops/issue-tracker/issues/6)
    * For loaner laptops: Do not hesitate when expensing a loaner laptop while your primary laptop is being repaired. Use your best judgment identifying a local vendor and share your experiences in this [issue](https://gitlab.com/gitlab-com/business-ops/it-ops/issue-tracker/issues/6)

### Currency Conversion
If you need help with calculating the conversion rate to $ USD, [please refer here.](https://www1.oanda.com/currency/converter/)
