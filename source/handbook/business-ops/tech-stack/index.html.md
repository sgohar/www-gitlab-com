---
layout: markdown_page
title: "Business Operations - Tech Stack Details"
---

## On this page
{:.no_toc}

- TOC
{:toc}  

## 1Password
[1Password](https://1password.com/) is a password manager. GitLab uses 1Passwords for Teams which is a hosted service. Please refer to the [1Password Guide](/handbook/security/#1password-guide) for additional information.

## ADP
We use [ADP](https://www.adp.com/) to process payroll for US Team Members.

## Amazon Web Services (AWS)
[Amazon Web Services](https://aws.amazon.com/) is a cloud platform.

## Avalara
We use [Avalara](https://www1.avalara.com/us/en/index.html) to automate and manage all of our sales tax compliance obligations.

## Balsamiq Cloud
[Balsamiq](https://balsamiq.cloud/) is a wireframing tool.

## BambooHR
[BambooHR](https://www.bamboohr.com/) is used to store all team member information in one central location. Additional information can be found in the PeopleOPS Handbook referencing [using BambooHR](/handbook/people-operations/#using-bamboohr).

## Betterment
[Betterment](https://www.betterment.com/) is an Online Investment and Financial Advisor for US Team Members.

## Bizible
[Bizible](https://www.bizible.com/) is our marketing attribution software that allows for connecting marketing and sales touch points over a prospect's or customer's lifecycle directly to revenue. It also allows us to directly tie the investment in our online advertising to records within our database.  Although Bizible has dashboards, we are not currently using the reporting to measure success or track marketing.  Bizible allows us to track our marketing activities at a more granular level within Salesforce, our single source of truth.

## Blackline
[Blackline](https://www.blackline.com/) develops cloud-based solutions to automate and control the entire financial close process. 

## Bonusly
GitLab uses [Bonusly](https://bonus.ly/) as our company wide recognition platform.

## Calendly
[Calendly](https://calendly.com/) connects to your Google Calendar so people outside GitLab can easily book a time with you. Additional information can be found in the [tools and tips](/handbook/tools-and-tips/#calendly) section of the GitLab Handbook.

## Carta
Stock option administration, capitalization table and equity management are all managed using the [Carta](https://carta.com/) software platform.

## Chef

## Chorus
Call and demo recording software. [Chorus](https://www.chorus.ai/) tracks keywords, provides analytics, and transcribes calls into both Salesforce and Outreach. Chorus will be used to onboard new team members, provide ongoing training and development for existing team members, provide non-sales employees with access to sales calls, and allow sales reps to recall certain points of a call or demo. At this time, US East, US West, US SDRs, US BDRs, and US Solutions Architects will be considered recorders. Everyone else can access the tool as a listener if they wish. 


### Getting Started
Once you have been granted access to Chorus, please make sure to activate your account. After you've done that, the very next step should be to link your Google Calendar. This is important so that the Chorus call recorder will be added automatically to any Zoom demo where you are the host. To link your Google account:

1. Go to `Settings`, which is the "gear" icon on the top right.
2. Go to `Personal Settings`, then in the sub-menu select `Calendar`.
3. Click `Link Google Calendar`
4. Select your GitLab Google account.
5. Click `Allow`

We will be updating the handbook as we continue with our onboarding, but in the mean time, here are some [Tips on Getting Started with Chorus](https://chorus.zendesk.com/hc/en-us/articles/115009183547-Tips-on-Getting-Started-with-Chorus). Please also refer to the short Chorus How-To videos available [here](https://about.gitlab.com/handbook/sales/training/#functional-skills--processes) on the GitLab Sales Training page.

## Clari
Opportunity forecasting and pipeline analysis tool. Clari will help the Sales Team improve forecast accuracy providing Sales Leadership with additional insight and details into an opportunity - insights that cannot be gleaned from Salesforce records or 1:1 forecasts calls alone. Additional insights include email activity between client/prospect and AE and stage movemement/velocity vs. historically won opportunities. Sales Leaders will also use Clari to conduct Manager/Rep 1:1s as well as the weekly forecast call with members of the executive team. We will also use Clari for the QBR presentations. Please see the [ Weekly Forecasting](/handbook/sales/#weekly-forecasting) section of the Sales Handbook for more information on how Clari will accomplish this.

## Conga Contracts
If a customer is requesting an editable version of any of our standard template agreements, the entire procecss will take place within Salesforce.com using [Conga Contracts](/handbook/business-ops/order-processing/#process-for-agreement-terms-negotiations-when-applicable).

## ContactOut
[ContactOut](https://contactout.com/) is a recruiting tool which helps recruiters find a potential candidate's contact information.

## ContractWorks
[ContractWorks](https://www.contractworks.com/) is a contract managing software. [This process](/handbook/legal/vendor-contract-filing-process/) is used to file contract or related vendor documents after they are fully executed.

## Cookiebot   
[Cookiebot](https://www.cookiebot.com/en/) is a cookie and online tracking consent solution that complies with the consent and information requirements of the EU ePrivacy Directive 2009/136/EC and the General Data Protection Regulation (GDPR). Cookiebot is a self-serve cloud service provided to you by the ePrivacy company Cybot.

## Crowdin.com
[Crowdin.com](https://translate.gitlab.com/) is a localization management platform. Additional information can be found on this [blog post](/2018/02/06/crowdin-localization-for-agile-projects/). 

## CultureAmp
[360 Feedback](/handbook/people-operations/360-feedback/) is completed via CultureAmp. [CultureAmp](https://gitlab.cultureamp.com) is a tool that makes it easy to collect, understand and act on team member feedback. It helps improve engagement, the experience and effectiveness of every team member. 

## Customers.GitLab.com
[Customers.GitLab.com](https://customers.gitlab.com) is the integration "platform" for customer payments between gitlab.com & Zuora.

## DataFox
[DataFox](https://www.datafox.com/) provides our Sales Reps with access to additional information in regards to our accounts such as number of employees, address, revenue, funding and other relevant data for the account. Most importantly DataFox is used to help set our account hierarchy structure by linking parent accounts with child accounts. 

## DigitalOcean
[DigitalOcean](https://www.digitalocean.com/) is a cloud based solution.

## DiscoverOrg  
[DiscoverOrg](https://discoverorg.com/) provides our Sales Development Representatives and Account Executives with access to hundreds of thousands of prospects and their contact information, company information, tech stack, revenue, and other relevant data. Individual records or bulk exports can be imported into Salesforce using extensive search criteria such as job function, title, industry, location, tech stack, employee count, and company revenue.  
![](/images/handbook/business-ops/tech-stack/DiscoverOrg-SFDC-homepage.png)

## Disqus
[Disqus](https://disqus.com/) is a blog comment hosting service for web sites and online communities that use a networked platform.

## Docusign
[Docusign](https://www.docusign.com/) enables people to electronically sign agreements.

## DoGood
We are using [DoGood](http://www.mydogood.com/) to help set meetings for account executives with leadership in target companies.

### Setting Meetings
We have notified DoGood of the individuals from their list that we would like to meet with. The outbound SDR team has supplied them with outreach email templates. DoGood will send emails to our desired contacts and see if they are interested.

### Accepting Meetings
If an individual expresses interest in meeting to learn more about GitLab, the SDR in charge of that account will create the contact in SFDC and ensure that the contact has the proper lead source and is attached to the appropriate account.

DoGood will have a short kickoff meeting with the account executive that will be taking the meeting, then make an introduction between the contact at the target company and the account executive.

### Tracking Meetings
The meeting and all further meetings with the contact will be tracked in SFDC as normal. After the completion of the meeting, we will report back to DoGood with one of three ratings:

1. Exceeded Expectations
2. Met Expectations
3. Did Not Meet Expectations

## Dribbble
[Dribbble](https://dribbble.com/) is a UX Prototyping application.

## Drift
[Drift](https://www.drift.com/) is a conversational marketing platform (live chat tool). Additional information on best practices can be found in the [Marketing Handbook](/handbook/marketing/marketing-sales-development/sdr/#drift-best-practices).

## Elastic Cloud
[Elastic Cloud](https://www.elastic.co/cloud/) is an [integration](https://docs.gitlab.com/ee/development/elasticsearch.html) service.

## Eventbrite
[Eventbrite](https://www.eventbrite.com/) is an event management and ticketing software application.

## Expensify
[Expensify](https://www.expensify.com/) is the company travel and expense management application. The platform provides employees a simple tool to create expense reports, which can then be managed from approval workflow through the final stage of reimbursement. 

## Fastly
[Fastly](https://www.fastly.com/) business purpose if used for CDN for production.

## FunnelCake
[FunnelCake](https://getfunnelcake.com/) is a MarketingOPS application.

## G-Suite
[G-Suite](https://gsuite.google.com/) is Google's brand of software and products.

## GitLab
[GitLab](https://about.gitlab.com/) is a single application for the entire software development lifecycle.

## GitLab Dev
[GitLab Dev](/handbook/engineering/infrastructure/environments/) is one of GitLab's multiple environments.

## Google Ads
[Google Ads](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#using-marketing-trend-data) provides data from Google, but the ranges are broad and terms can be combined into a single phrase.

## Google Analytics
[Google Analytics](https://www.google.com/analytics/) captures all the analytics for GitLab pages outside of the actual app on https://gitlab.com. If you need access to Google Analytics, you can request access from the [Online Marketing Manager](/job-families/marketing/digital-marketing-programs-manager/) or [Marketing Operations Manager](/job-families/marketing/marketing-operations-manager/).    

## Google Cloud Platform
Additonal information on how GitLab utilizes [Google Cloud Platform](https://cloud.google.com/) can be found [here](/solutions/google-cloud-platform/).

## Google Search Console
[Google Search Console](https://search.google.com/search-console/about) tracks and reports GitLab's site traffice and performance.

## Google Tag Manager
[Google Tag Manager](https://www.google.com/analytics/tag-manager/) is used to manage the firing of the different JavaScript tags we deploy on our website. All marketing tags should be deployed through Google Tag Manager except in cases such as A/B testing software. If you need access to Google Tag Manager, you can request access from the [Online Marketing Manager](/job-families/marketing/digital-marketing-programs-manager/) or [Marketing Operations Manager](/job-families/marketing/marketing-operations-manager/).

## GovWin IQ
[GovWin IQ](https://iq.govwin.com/login/loginPage.cfm?) is a prospecting and enrichment application.

## Grafana

## Greenhouse
[Greenhouse](http://www.greenhouse.io/) is GitLab's ATS (Applicant Tracking System). All hiring managers and interviewers will use Greenhouse to review resumes, provide feedback, communicate with candidates, and more. Please refer to the [hiring handbook](/handbook/hiring/greenhouse/) for additional information.

## HackerOne
[HackerOne](https://www.hackerone.com/) is GitLab's Bug Reports and Bug Bounty Site.

## HelloSign
[HelloSign](https://www.hellosign.com/) is used for signing edocuments.

## JetBrains
[JetBrains](https://www.jetbrains.com/) is a code editor used by our Engineering department. 

## LeanData  
When a lead is created in Salesforce, [LeanData](https://www.leandatainc.com/) will be the tool that routes it to the appropriate user. Routing rules include sales segmentation, region, lead source, and owned accounts. For example, if a lead from a named account is created, it will be routed directly to the owner of the named account. Also, LeanData provide cross-object visibility between leads and accounts and contacts. When in an account record, a user can view "matched" leads by company name, email domain, and other criteria.

### What is LeanData?

LeanData enables strategic lead management within Salesforce so leads are assigned to the right reps. This is done by leveraging LeanData's fuzzy logic algorithms and lead-matching database. Whether we introduce a sales process based on named accounts, geographic territories, employee size (or all of the above), LeanData allows us to easily manage seemingly complex lead assignment rules.

### How It Works

LeanData is made up of three primary solutions: Lead/Account Matching, Lead Routing Rules, and Cross-Object Visibility.

#### Lead/Account Matching

The primary feature for LeanData is matching leads to accounts using a number of different criteria: company name, email address, and IP address. If a new lead is created in Salesforce, LeanData will review the criteria to determine if it matches an existing account in the system and will deliver the lead to the correct user as per our defined lead routing rules. Note that our matching criteria will include both the company information and region.   

#### Cross-Object Visibility

One of the most widely-known issues with the Salesforce architecture is the lack of relationship between the lead and account/contact objects. LeanData addresses this lack of functionality. A user can be in an account record and see all related lead records. Within the lead object, a user can view all related leads and accounts.

**LeanData from the Account Page**
 ![](/images/handbook/business-ops/tech-stack/leandata-contact.png)

* Merging a Duplicate Account
  * At this time, this feature is not supported. Please send a note via Chatter on the record you would like merged: "Please merge this account with `<URL of original account>`."

* Converting a Lead from the Account Page
  * Click on the "Convert" link for the lead you would like to add to the Account.
  * You can assign the contact to yourself or another user.
  * The account name will default to the account record you were just on. If you want to create a new account, select the "Create New Account" option.
  * "Do not create a new opportunity upon conversion" is defaulted to TRUE. If you want to create a new opportunity, uncheck this box.
  * Click the "Convert" button.

**LeanData from the Lead Page**
 ![](/images/handbook/business-ops/tech-stack/leandata-lead.png)

 * Merging a Duplicate Lead
  * Before you merge, please visit a Salesforce article regarding merging leads, ["Considerations for Merging Duplicate Leads"](https://help.salesforce.com/articleView?id=leads_merge_considerations.htm&type=0&language=en_US).
  * Click on the "Merge" link for the lead you would like to merge to the current Lead.
  * Select one lead as the “Master Record.” The created date, created by, and read-only fields will retain information from the Master Record.
  * Select the fields that you want to retain from each record.
  * Click "Merge".
  * Click "OK".

 * Converting a Lead from the Lead Page
  * Click on the "Convert" link for the lead you would like to add to the Account.
  * You can assign the contact to yourself or another user.
  * The account name will default to the account record you were just on. If you want to create a new account, select the "Create New Account" option.
  * "Do not create a new opportunity upon conversion" is defaulted to TRUE. If you want to create a new opportunity, uncheck this box.
  * Click the "Convert" button.  

#### Vacation Setting in LeanData  
You can schedule someone to be placed on "vacation hold" in a round robin group. There always has to be one member of a round robin group that is active. If you have access to LeanData you can set a vacation hold by doing the following:   
1. In SFDC, navigate to the LeanData tab   
2. Click on `Routing` -> `Round Robin` -> `Schedules`  
3. In the middle of the page, you will see three selections `Working Hours`, `Holiday Sets` and `Vacations`. Select `Vacations`  
4. Click the large green button that says "Schedule Vacations".
5. The next screen will appear blank if there are no vacations currently scheduled. To schedule one, click the large green button "Add Vacation".  
6. In the first drop down, select the `User Name` (any active SFDC user will be in this field).  
7. Give the vacation a name.  
8. Select the Start Date & Time as well as the End Date & Time 
9. When you are done click the large green button on the far right "Done". 
10. The system will automatically pull this person out of the round robin during this period and add them back in when the vacation hold has expired. 

## LicenseApp
[LicenseApp](/handbook/engineering/projects/#license-app) is an internal GitLab Rails app to generate license keys for EE.

## LinkedIn Recruiter
[LinkedIn Recruiter](https://business.linkedin.com/talent-solutions/recruiter) is a platform for finding, connecting with, and managing candidates.

## LinkedIn Sales Navigator
[LinkedIn Sales Navigator](https://www.linkedin.com/sales/login) is a prospecting tool & extended reach into LinkedIn Connections.

## LucidChart

## Lumity
US Team Member benefits are arranged through [Lumity](https://lumity.com/).

## MailChimp
[MailChimp](https://mailchimp.com/) is a marketing program.

## MailGun
[MailGun](https://www.mailgun.com/) is an email API.

## Marketo  
[Marketo](https://www.marketo.com/) is our marketing automation platform managed by our Marketing Ops team. Anyone who signs up for a trial, requests contact from GitLab, attends a webinar or trade show, or engages in any other marketing activity, will end up in Marketo. These prospects will receive scores based on their level of engagement and these scores will be used by the Business Developement Representatives and Account Executives to prioritize which prospects to follow up with. Marketo is also our primary tool for delivering marketing communication to prospects and customers.  

## Meetup
[Meetup](https://www.meetup.com/)is an event management platform.

## Modern Health
[Modern Health](https://www.joinmodernhealth.com/) is an Employee Assistance Program offered to all team members.

## Moo
[Moo](https://www.moo.com/us/) is the system GitLab uses to place business cards.

## Moz Pro  
[Moz Pro](https://moz.com/products/pro) is our all-in-one SEO tool set used by the Online Growth team for analysis of our website, effectiveness of keywords, optimization of landing pages and tracking our site rankings.  

## Mural
[Mural](https://mural.co/) is a visual collaboration tool.

## NetSuite
[NetSuite](http://www.netsuite.com/portal/home.shtml?noredirect=T) is the company Enterprise Resource Planning (ERP) system, which is primarily managed by the Finance team. The platform allows enhanced dimensional reporting as well as multi-currency and multi-entity reporting. This is where the General Ledger resides and all financial activity is ultimately recorded, which is critical to reporting the financial health of the company.

## NexTravel
[NexTravel](https://www.nextravel.com/) is a corporate travel management software.

## Okta
[Okta](/handbook/business-ops/okta/) is an Identity and Single Sign On solution for applications and Cloud entities. It allows GitLab to consolidate authentication and authorisation to Applications we use daily through a single dashboard and ensure a consistent, secure and auditable login experience for all our staff.

## Ops - GitLab
[Ops - GitLab](https://ops.gitlab.net/users/sign_in) is internal GitLab for Operations.

## Optimal Workshop
[Optimal Workshop](https://www.optimalworkshop.com/) is a user research platform.

## Outreach.io  
[Outreach.io](https://www.outreach.io/) is a tool used to automate emails in the form of sequences. Users can track open rates, click through rates, response rates for various templates and update sequences based on these metrics. Outreach.io also helps to track sales activities such as calls. All emails/calls/tasks that are made through Outreach.io are automatically logged in Salesforce with a corresponding disposition. See below for a list of current call dispositions, what they mean and scenarios on when to use each of them.

| Outreach.io Disposition | Meaning/Scenarios |
|---|---|
`Correct Contact: Answered`| The correct contact actully picked up the phone and you had a conversation with the contact|
|`Correct Contact: Left Message`| You were able to reach the voicemail for the correct contact and you left a message on their machine or with their Personal Assistant |
|`Correct Contact: Not Answered/Other`| You were able to reach the correct contact through a company directory but it kept ringing. You reached the contacts voicemail but their voicemail was not set up so you could not leave a message |
|`Busy`|Get a busy tone when calling|
|`Bad Number`|The phone number is not valid|
|`Incorrect Contact: Answered`| The wrong person answered the phone number that you had for this contact and it is the wrong persons phone number (They were not a personal assistant). They didn't take a message for the correct person or give helpful information|
|`Incorrect Contact: Left Message`|The wrong person answered the phone and it is the wrong persons phone number (They were not a personal assistant). They took a message for the correct person/gave you the correct number for the contact|
|`Incorrect Contact: Not Answered/Other`| You got through to the voicemail but the voicemail was for someone other than the person who you were trying to contact. Or the person was not listed in the company directory and you were calling the companies main number|

### Sending Email Using Outreach   

Outreach is **not** meant for mass communications nor bulk email sends it is intended for very targeted account and prospect communications. The Outreach platform is directly integrated to the GitLab Gmail account and each users email is linked through OAuth therefore all activity in Outreach has the potential to impact the IP reputation of the GitLab domain with all major email services providers.   

As such there are [sending limits built into the Outreach platform](https://support.outreach.io/hc/en-us/articles/205022518-Individual-Email-Limits-Safeguards) as well as [limits put in place by Gmail itself](https://support.google.com/a/answer/166852?hl=en).   

#### Sending Limits   
* Individual users can send up to 2,000 emails *combined* between Outreach and Gmail inbox in a rolling 24-hour period.    
    * This is the maximum across both systems, if you max out in Outreach, you **will be** maxed out in Gmail.   
* Outreach has a **hard limit** of 5,000 emails in a rolling seven day period.  
    * If you max out in Outreach, your emails will be queued to try again in 24 hours, when your account drops below this hard limit.


### External Resources
* [Outreach University](http://university.outreach.io/)

## OwnBackup
[OwnBackup](https://www.ownbackup.com/) is a SalesForce backup tool. We use it to take backups of both data and metadata nightly for the production instance of Salesforce. 

### Backup Strategy
* We do a full backup of data once per week and incremental backups (stored as Synthetic Full backups) every 24 hours. 
* We do full backups of all metadata (Object definitions, Apex Classes, Templates, Layouts, Permissions, etc.) every 24 hours. 
* Partial restores to a Sandbox will be done by the Business Operations team once per quarter to ensure our ability to restore to a workable state following a disaster. 

## PackageCloud
[PackageCloud](https://packages.gitlab.com/)

## PagerDuty
[PagerDuty](https://gitlab.pagerduty.com/sign_in) is an Operational Alerting Tool. We use PagerDuty to set the on-call schedules, and to route notifications to the correct on-call hero.

## PathFactory
[PathFactory](https://www.pathfactory.com/) is used create content tracks and micro-personalized website recommendations.

## Periscope
[Periscope](https://www.periscopedata.com/) is our Data Visualization and Business Intelligence tool. It is for reporting and can have its dashboards embedded into Salesforce, the Handbook, or other places where GitLab team-members already do their work. Please read more in the [Periscope Directory](/handbook/business-ops/data-team/periscope/).

## Qualtrics
[Qualtrics](https://www.qualtrics.com/) is used for customer surveys.

## Rackspace

## Rollup Helper
[Rollup Helper](https://www.passagetechnology.com/) is a Salesforce productivity tool and is used to roll-up any Salesforce data: Count, sum, max, min, average, percent, lookups, text, formula, and multi-currency.

## Salesforce   
[Salesforce](https://www.salesforce.com/) is our CRM of record. It integrates with all of the other applications in our business tech stack. Salesforce stores prospect, customer, and partner information. This includes contact information, products purchased, bookings, support tickets, and invoices, among other information.

### Standard Object Definitions

#### Lead   
A lead is a prospect who has yet to be qualified. Lead may have expressed interest by requesting contact, starting a trial, attending a webinar or trade show, or registering on GitLab.com or our customer portal. Leads are typically followed up by our Business Development Reps (BDR) or Account Executives. During this initial qualification process, a BDR will uncover certain details that will determine whether the prospect is qualified. For more information on the BDR Qualification process, [click here](/handbook/marketing/marketing-sales-development/sdr/#qualifying). Once qualified, the lead is converted into an Account, Contact, and Opportunity. For all non-marketing and non-system administrators of Salesforce, all leads are read-only.

#### Account  
An account can either be a prospect who was qualified by a BDR or Account Executive, an existing customer, or a partner. Accounts will contain all company information, including billing and shipping address, company phone, website, support package, industry, employee count, and annual revenue. An account will be the parent record for associated contacts, opportunities, subscriptions, invoices, and payments.

#### Contact   
A contact is a person associated to an account record. Information contained in the contact record include name, mailing address, title, role, phone, and email. An account can have many contacts associated to it. Contacts can be added to opportunities via [Contact Roles](https://help.salesforce.com/articleView?id=000005632&type=1&language=en_US).

#### Opportunity   
An opportunity is created when a prospect has expressed interest in GitLab. The opportunity record includes information regarding close dates, contract value, and type. An opportunity will be in a certain stage, depending on where it is in the sales cycle. For more information on stages, [click here](/handbook/sales/#opportunity-stages).

#### Tasks and Events   
Tasks and events are used when logging calls and meetings into Salesforce. Collectively called "activities", these records are meant to track interactions with customers and prospects. For example, you might create a task after you've attempted to call a prospect. You may also want to create a task as a reminder to complete a certain task, such as call a prospect or send an email. Tasks can also be created by external systems such as Marketo or Outreach when emails are sent to leads or contacts. External resources: [Difference Between a Task and an Event](https://success.salesforce.com/answers?id=90630000000gvEbAAI), [SFDC Task Documentation](https://help.salesforce.com/articleView?id=activities.htm&r=https%3A%2F%2Fwww.google.com.ph%2F&type=0)

### External Resources
* [Salesforce Success Community](https://success.salesforce.com/)
* [Salesforce Help](https://help.salesforce.com/home)
* [Salesforce Glossary](https://help.salesforce.com/articleView?id=glossary.htm&type=0)


#### Commonly Asked Questions
* [How to Create a Report](https://help.salesforce.com/articleView?id=reports_builder_create.htm&type=0)
* [How to Create a List View](https://help.salesforce.com/articleView?id=customviews.htm&type=0&language=en_US&release=208.6)   


## Screaming Frog
[Screaming Frog](https://about.gitlab.com/handbook/business-ops/tech-stack/#screaming-frog)

## Sertifi  
[Sertifi](https://corp.sertifi.com/) is used when a quote or other document requiring signature needs to be delivered to a prospect.

## Shopify

## Sigstr
[Sigstr](https://www.sigstr.com/) is an email signature marketing platform used to standardize brand consistency.

## Sketch
[Sketch](https://www.sketch.com/) is a design toolkit.

## Slack
[Slack](https://slack.com/) is an internal communications tool.

## Snowflake
[Snowflake](https://gitlab.snowflakecomputing.com) is our primary data warehouse. It is the single source of truth for all company data.

## Snowplow

## Sprout Social
[Sprout Social](https://sproutsocial.com) is a Social Media Management application.

## staging.gitlab.com
[staging.gitlab.com](https://staging.gitlab.com/) is the GitLab staging server.

## Status-IO
GitLab uses [Status-IO](https://status.io/) to continously monitor GitLab.com.

## Stitch
GitLab uses [Stitch](https://www.stitchdata.com/) to extract data from its raw sources and moves it into Snowflake. 

## Stripe
[Stripe](https://stripe.com/) is a software application that enables GitLab customers to make online payments. Finance uses this system to collect information pertaining to payments made online.

## Survey Monkey
[Survey Monkey](https://www.surveymonkey.com/) is an online survey tool.

## Tenable.IO
[Tenable.IO](https://www.tenable.com/products/tenable-io) is a vulnerability management application.

## Tipalti
[Tipalti](https://tipalti.com/) is an account payable automation tool.

## TheHive
[TheHive](https://thehive-project.org/) is a security operations platform.

## Tweetdeck
[TweetDeck](https://tweetdeck.twitter.com/) is a social media dashboard application for management of Twitter accounts.

## UsabilityHub
[UsabilityHub](https://usabilityhub.com/) is used to build design evaluations, such as first click tests, preference tests and five second tests. Product Designers share a login for UsabilityHub. The credentials are stored in 1Password

## Visual Compliance
[Visual Compliance](https://www.visualcompliance.com/) is a opportunity and account screening tool. It screens opportunities in Salesforce to ensure that Gitlab does not violate any US Regulations by selling to restricted individuals or organizations. 

## WebEx
[WebEx](https://www.webex.com/) is an online meeting application. GitLab primarily utilizes Zoom but WebEx [is available for individuals](/handbook/tools-and-tips/#webex) who prefer to WebEx to Zoom.

## Will Learning
[Will Learning](https://launchpad.willinteractive.com/users/sign_in) is a Workplace Training application.

## Xactly
[Xactly](https://www.xactlycorp.com/) is used for tracking and payment of variable compensation including sales commissions payment based on Closed Won Opportunities.

## YouTube
[YouTube](https://youtube.com) is a video sharing platform. 

## Zapier
[Zapier](https://zapier.com/) is an automation application integrating other applications.

## Zendesk   
Customer support tickets are managed through [Zendesk](https://www.zendesk.com/) and can be visible in the Lead, Account, or Opportunity object in Salesforce in the form of a Visualforce page. This provides visibility for non-Zendesk users into the support tickets created by their prospects and customers. For more information on this tool, you can visit the Support page [here](/handbook/support).

## Zoom
[Zoom](https://zoom.com/) is a video conference and meeting application.

## Zuora   
[Zuora](https://www.zuora.com/) is our subscription management and billing system, primarily managed by our Finance team. A quote object within Salesforce is created with a specific configuration, and an order form (PDF) is generated. Once the order form is signed, the Quote in Salesforce is converted to a Subscription in Zuora and the invoice is delivered or payment is applied if the customer provides their credit card details.
