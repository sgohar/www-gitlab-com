---
layout: markdown_page
title: "People Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Need Help?
{: #reach-peopleops}

Welcome to the People Group handbook! You should be able to find answers to most of your questions here. You can also check out [pages related to People Group](/handbook/people-operations/#other-pages-related-to-people-operations) in the next section below. If you can't find what you're looking for please do the following:

- [**The People Group**](https://gitlab.com/gitlab-com/people-ops) holds several subprojects to organize the people operations team; please create an issue in the appropriate subproject or `general` if you're not sure. Please use confidential issues for topics that should only be visible to GitLab team-members. Similarly, if your question can be shared please use a public issue. Tag `@gl-peopleops` or `@gl-hiring` so the appropriate team members can follow up.
  * Please note that not all People Group projects can be shared in an issue due to confidentiality. When we cannot be completely transparent, we will share what we can in the issue description, and explain why.
  * [**Employment Issue Tracker**](https://gitlab.com/gitlab-com/people-ops/employment/issues): Only Onboarding, Offboarding and Management Onboarding Issues are held in this subproject, and they are created by People Ops specialists only. Interview Training Issues, are held in the [Training project](https://gitlab.com/gitlab-com/people-ops/Training) and created by the Recruiting team. Please see the [interviewing page](/handbook/hiring/interviewing/#typical-hiring-timeline) for more info.
- [**Chat channel**](https://gitlab.slack.com/archives/peopleops); Please use the `#peopleops` slack chat channel for questions that don't seem appropriate for the issue tracker. For access requests regarding Google or Slack groups, please create an issue here: https://gitlab.com/gitlab-com/access-requests. For questions that relate to ADP, Payroll, Invoicing, or Carta, please direct your questions to the `#finance` channel. Regarding questions for our recruiting team, including questions relating to access, or anything to do with Greenhouse, referrals, interviewing, or interview training please use the `#recruiting` channel. For more urgent general People Operations questions, please mention `@peoplegeneral` to get our attention faster.
- If you need to discuss something that is confidential/private, you can send an email to the People Group (see the "Email, Slack, and GitLab Groups and Aliases" Google doc for the alias).
- If you only feel comfortable speaking with one team member, you can ping an individual member of the People Group team, as listed on our [Team page](/company/team/).
- If you need help with any technical items, for example, 2FA, please ask in `#it-ops` and mention `@it-ops-team`.

## How to reach the right member of People Operations

This table lists the aliases to use, when you are looking to reach a specific group in the People Group. It will also ensure you get the right attention, from the right team member, faster.

| Subgroup                          | GitLab handle   | Email            |   
|-----------------------------------|-----------------|------------------|
| [People Business Partners](https://gitlab.com/gitlab-com/people-ops/General) | @gl-peoplepartners | peoplepartners@domain |  
| [Compensation and benefits](https://gitlab.com/gitlab-com/people-ops/Compensation) | @gl-compensation | compensation@domain |     
| [People Operations General](https://gitlab.com/gitlab-com/people-ops/General) | @gl-peopleops  | peopleops@domain |
| [Diversity and Inclusion](https://gitlab.com/gitlab-com/diversity-and-inclusion) | No alias yet, @mention the [Diversity and Inclusion Partner](/job-families/people-ops/diversity-inclusion-partner/) | diversityinclusion@domain | 
| [Web Content/ Handbook](https://gitlab.com/gitlab-com/people-ops/General) | No alias yet, @mention the [Web Content Manager](/job-families/people-ops/web-content-manager/)               |  handbook@domain | 
| [Learning and Development](https://gitlab.com/gitlab-com/people-ops/Training) | No alias yet, @mention the [Learning and Development Generalist](/job-families/people-ops/learning-development-specialist/) | learning@domain | 
| [Recruiting](https://gitlab.com/gitlab-com/people-ops/recruiting) | @gl-recruiting | recruiting@domain | 
| [Candidate Experience Specialist](https://gitlab.com/gitlab-com/people-ops/recruiting) | @gl-ces | ces@domain |
| [Employer Branding](https://gitlab.com/gitlab-com/people-ops/recruiting) | No alias yet, @ mention the [Employer Branding Lead](/job-families/people-ops/employment-branding-specialist/) | employmentbranding@domain | 

## Other pages related to People Operations

- [Benefits](/handbook/benefits/)
- [Code of Conduct](/handbook/people-operations/code-of-conduct/)
- [Promotions and Transfers](/handbook/people-operations/promotions-transfers/)
- [Global Compensation](/handbook/people-operations/global-compensation/)
- [Incentives](/handbook/incentives)
- [Hiring process](/handbook/hiring/)
- [Group Conversations](/handbook/people-operations/group-conversations)
- [Leadership](/handbook/leadership/)
- [Learning & Development](/handbook/people-operations/learning-and-development/index.html)
- [Onboarding](/handbook/general-onboarding/)
- [Offboarding](/handbook/offboarding/)
- [OKRs](/company/okrs/)
- [People Group Vision](/handbook/people-operations/people-ops-vision)
- [360 Feedback](/handbook/people-operations/360-feedback/)
- [Guidance on Feedback](/handbook/people-operations/guidance-on-feedback)
- [Collaboration & Effective Listening](/handbook/people-operations/collaboration-and-effective-listening/)
- [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-operations/gender-pronouns/)
- [Travel](/handbook/travel/)
- [Underperformance](/handbook/underperformance)
- [Visas](/handbook/people-operations/visas)

## Role of the People Group
{: #role-peopleops}

In general, the People Group team and processes are here as a service to the rest of the team; helping make your life easier so that you can focus on your work and contributions to GitLab. On that note, please don't hesitate to [reach out](/handbook/people-operations/#reach-peopleops) with questions! In the case of a conflict between the company and a team member, People Operations works "on behalf of" the company.

## Team Directory
{: #directory}

GitLab uses Slack profiles as an internal team directory, where team members can add their personal contact details, such as email, phone numbers, or addresses. This is your one-stop directory for phone numbers and addresses (in case you want to send your team mate an awesome card!). Feel free to add your information to your Slack profile (this is completely opt-in!) by clicking on "GitLab" at the top left corner of Slack, "Profile & Account", then "Add Profile" (for the first time making changes) or "Edit Profile" (if your account is already set up) to make any changes!

- Please make sure that your address and phone information are written in such a way that your teammates can reach you from a different country. So, for example, include `+[country code]` in front of your phone number.

## Anniversary Swag 

Once per week, GitLab team-members with a hire-date anniversary will get a post in the `#celebrations` channel on Slack so all team members can help them celebrate.

Work Anniversary Gifts are as follows:
* Year 1:  Custom mug with a representation of your commits in GitLab
* Year 3:  GitLab Hoodie/Jacket 
* Year 5:  GitLab travel bag/ backpack
* Year 10: $500 Travel grant/ airbnb gift card

How to order anniversary gifts:
1. Create the minimum order. A minimum order consist of: 72 mugs, 10 jackets and 25 bags. This will be stored in the Supplier's warehouse and shipped when needed.
2. Complete the order sheet [sheet](https://docs.google.com/spreadsheets/d/1wL-M4YN1L0s8GwB68BrMjOU6QBmXjgPeYeB8da352oo/edit?ts=5d5bad32#gid=0) with all anniversaries from August 20th 2019. This order sheet is shared with our supplier in order for them to complete the shipping.
3. Email our supplier on the 1st day of every month.
5. If you have not received your anniversary gift within 8 weeks (please consider global shipping) of your anniversary, please email peopleops@gitlab, so they can follow up.

## Birthdays

The company encourages all GitLab team-members to take a day of vacation on their birthday by utilizing our [paid time off](/handbook/paid-time-off/) policy.

## Letter of Employment and Reference Request

If you need a letter from GitLab verifying your employment/contractor status, please send an email at peopleops@domain and cite what information is needed. We will provide the most recent title, dates of employment, and salary information. We will also verify (but not provide) National Identification Numbers. People Operations Specialists will send you the letter once it is completed. In addition, if the request comes from a third party, People Operations Specialists will always verify that the information is appropriate to share.

You do not need permission from GitLab to give a personal reference, but GitLab team members are not authorized to speak on behalf of the company to complete reference requests for GitLab team members no longer working for GitLab. If a team member would like to give a personal reference based on their experience with the former team member, it must be preceded by a statement that the reference is not speaking on behalf of the company. To reinforce this fact, personal references should never be on company letterhead, and telephone references should never be on company time. Remember to always be truthful in reference checks, and instead of giving a majority negative reference, refuse to provide one. Negative references can result in legal action in some jurisdictions.

If an ex team member acted in a malicious way against GitLab we'll do a company wide announcement on the company call not to provide a reference.

## Boardroom addresses
{: #addresses}

- For the SF boardroom, see our [visiting](/company/visiting/) page.
- For the NL office, we use a postbox address listed in the "GitLab BV address" note in the Shared vault on 1Password. We use [addpost](https://www.addpost.nl) to scan our mail and send it to a physical address upon request. The scans are sent via email to the email alias listed in the "Email, Slack, and GitLab Groups and Aliases" Google doc.
- For the UK office, there is a Ltd registered address located in the "GitLab Ltd (UK) Address" note in the Shared vault on 1Password
- For the Germany office, there is a GmbH address located in the "GitLab GmbH Address" note in the Shared vault on 1Password

## Business Cards

Business cards can be ordered through Moo. Please let People Operations Specialists know if you have not received access to your account on Moo by the end of your first week with GitLab. Place the order using your own payment and add it to your expense report. If you need any assistance, let a People Operations Specialist know.

Once you are logged in, follow these steps to create your cards:

1. Select the "+" sign in the upper right corner of your screen. (If you don't see the "+" sign then go to [this URL](https://www.moo.com/mbs/products/business-cards)).
1. Select your currency in the upper right corner to ensure that your shipment is sent from the correct location.
1. Select "Business Cards".
1. Select your template (one has the Twitter & Tanuki symbol and cannot be removed, and one is free of those symbols).
1. Enter your information into the card.
1. Please remember to choose rounded corners.
1. Add the card to your cart and order! We ask for a default of 50 cards unless you are meeting with customers regularly.
1. Add the cards to your expense report under 'office supplies'.

### Business Cards - India

Since MOO ships business cards from outside India via DHL, DHL is mandatorily required to perform "Know Your Customer" (KYC) checks before delivery.
If you are a team member residing in India, please consider using the following tips while ordering business cards from MOO.

- Avoid filling in the "Company name" field when checking out your order to prevent the shipment being sent in the company's name instead of your name. It is only necessary to fill the "First Name" and "Last Name" fields.
- For verification, DHL matches the name and address on your proof of ID with the name and address in your consignment. So, when providing the address for delivery, make sure to provide the same address as on one of your proofs of ID.
- In a scenario where you do not have any ID associated with the address you intend to provide, consider shipping the item in the name of somebody who holds a valid proof of ID at that address, like a relative or a friend.
- Please check out the list of [valid KYC documents](https://dhlindia-kyc.com/forms/valid-kyc-docs.aspx#ind-indian) before placing the order.

## NPS Surveys

NPS stands for "Net Promoter Score". GitLab has two forms of NPS surveys: eNPS for all employees (where "e" stands for "Employee"), and an onboarding NPS. These surveys gauge employee satisfaction and how likely employees are to recommend GitLab to others as a place to work.

People Ops will send out an eNPS survey twice yearly to all employees.

The onboarding NPS survey is a 60-day survey for new hires. To help People Ops understand your experience and improve the onboarding process, please complete the [onboarding survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) after you have been working for GitLab for at least 2 months. People Ops will send reminders to team members to complete the survey.

## Gifts

People Operations will send flowers for a birth, death, or other significant event for a team member. This policy applies to an immediate relationship to the GitLab team-member. Managers can send a request to People Operations in order to ensure that a gift is sent in the amount of [75-125 USD](/handbook/people-operations/global-compensation/#exchange-rates).

## Name Change

To initiate a change in names please complete the following:

- Update ID in BambooHR
- Email peopleops@gitlab.com, compensation@gitlab.com, and payroll@gitlab.com
- Open an access request issue to change name and email address in Google.

## Using BambooHR

We use [BambooHR](https://gitlab.bamboohr.com) to keep all team member information
in one place. All team members (all contract types) are in BambooHR.
We don't have one contact person but can email BambooHR if we want any changes made in the platform. The contact info lives in the Secretarial Vault in 1Password.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action, prepare a communication to the team that is approved by the Chief People Officer.

Team Members have employee access to their profile in BambooHR and should update any data that is outdated or incorrect. If there is a field that cannot be updated, please reach out to the People Ops Analyst with the change.

The mobile app has less functionality than the full website in regards to approvals. Only time off requests can be approved through the mobile app. If other types of requests need to be approved, those would have to be done through the desktop version of the website. 

## Using RingCentral

Our company and office phone lines are handled via RingCentral. The login credentials
are in the Secretarial vault on 1Password. To add a number to the call handling & forwarding
rules:

- From the Admin Portal, click on the Users button (menu on left), select the user for which you
  want to make changes (for door handling, pick extension 101).
- A menu appears to the right of the selected user; pick "Call Handling & Forwarding" and review
  the current settings which show all the people and numbers that are alerted when the listed User's
  number is dialed.
- Add the new forwarding number, along with a name for the number, and click Save.

## WBSO (R&D tax credit) in the Netherlands

For roles directly relating to Research and Development in the Netherlands, GitLab may be eligible for the [WBSO (R&D Tax Credit)](http://english.rvo.nl/subsidies-programmes/wbso).

### Organizing WBSO

**Applications**

As of 2019 GitLab must submit three applications each year and the deadlines for those are as follows:

1. **31 March 2019**, for the May - August 2019 period (Product Manager for Create Features)
1. **31 August 2019**, for the September - December 2019 period (Product Manager for Gitaly)
1. **30 November 2019**, for the January - April 2020 period (Product Manager for Geo Features)

There is a [translated English version of the application template](https://docs.google.com/document/d/15B1VDL-N-FyLe84mPAMeJnSKjNouaTcNqXeKxfskskg/edit) located in the WBSO folder on the Google Drive. The applications should be completed by a Product Manager, responsible for features or a service within GitLab, who can detail the technical issues that a particular feature will solve. Assistance on completing the application can also be sought from the WBSO consultant (based in the Netherlands). The contact details for the consultant can be found in a secure note in the People Ops 1Password vault called WBSO Consultant. People Operations will assist with co-ordinating this process. It is currently owned by Finance.

**Hour Tracker**

Each year a spreadsheet with the project details and hours logged against the work done on the project(s) will need to be created. This is for the entire year. The current hour tracker is located in the WBSO folder on the Google Drive, and shared only with the developers that need to log their hours (located in the Netherlands), People Operations Analysts, Finance and the WBSO consultant. Once the projects have been completed for the year, the WBSO consultant will submit the hours and project details to claim the R&D grant from the [RVO](https://english.rvo.nl/). The WBSO consultant will contact People Operations Analysts should they have any queries.

## Paperwork people may need to obtain a mortgage in the Netherlands

When your employment contract is for a fixed period of time (e.g. 12 months) you'll need a "werkgeversverklaring".
This document describes your salary and states that your employer expects to continue to employ
you after the contract expires (assuming the performance of the employee doesn't degrade).
This document has to be filled in by hand, preferably using blue ink, and must be signed
and stamped. If there is no stamp (as is the case for GitLab) an extra letter (also signed)
must be supplied that states the employer has no stamp. While the language of these
documents doesn't matter, the use of Dutch is preferred.

Employees also have to provide a copy of a payslip that clearly states their
monthly salary and also their annual salary. These numbers must match the numbers on
the "werkgeversverklaring" down to the decimals. Mortgage providers may also require
you to provide information about your financial status, usually in the form of a report/screenshot
of your total financial status (including any savings you have). The requirements for
this seem to vary a bit between mortgage providers.
